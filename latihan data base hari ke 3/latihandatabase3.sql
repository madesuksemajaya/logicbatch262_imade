create database DB_HR
use DB_HR
create table tb_devisi(
id int primary key identity(1,1)not null,
kd_divisi varchar (50) not null,
nama_divisi varchar(50) not null,
)
create table tb_jabatan(
id int primary key identity(1,1)not null,
kd_jabatan varchar (50) not null,
nama_jabatan varchar(50) not null,
gaji_pokok int,
tunjangan_jabatan int
)

create table tb_karyaawan(
id int primary key identity(1,1)not null,
nip varchar (50) not null,
nama_depan varchar(50) not null,
nama_belakang varchar(50) not null,
jenis_kelamin varchar(50) not null,
agama varchar(50) not null,
tempat_lahir varchar(50) not null,
tgl_lahir date null,
alamat varchar(100) not null,
pendidikan_terakhir varchar(50) not null,
tgl_masuk date null
)

create table tb_pekerja(
id int primary key identity(1,1)not null,
nip varchar (50) not null,
kode_jabatan varchar(50) not null,
kode_devisi varchar(50) not null,
tunjangan_kinerja int,
kota_penempatan varchar(50)
)

insert into tb_devisi
(kd_divisi,nama_divisi)
values
('GD','gudang'),
('HRD','HRD'),
('KU','keuangan'),
('GD','gudang'),
('UM','umum')

insert into tb_jabatan
(kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan)
values
('MGR','manager',5500000,1500000),
('OB','office boy',1900000,200000),
('ST','staff',3000000,750000),
('WMGR','wakil manager',4000000,1200000)

insert into tb_karyaawan
(nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk)
values
('001','hamidi','samsudin','pria','islam','sukabumi','1997-04-21','jl.sudirman No.12','s1 teknik mesin','2015-12-07'),
('002','ghandi','wamida','wanita','islam','palu','1992-01-12','jl.rambutan no.22','sma negeri 02 palu','2014-12-01'),
('003','paul','christian','pria','kristen','ambon','1980-05-27','jl.veteran no.4','s1 pendidikan geografi','2014-01-12')

select * from tb_devisi

insert into tb_pekerja
(nip,kode_jabatan,kode_devisi,tunjangan_kinerja,kota_penempatan)
values
('001','ST','KU',750000,'cianjur'),
('002','OB','UM',350000,'sukabumi'),
('003','MGR','HRD',1500000,'sukabumi')


--1.Tampilkan nama lengkap, nama jabatan, total tunjangan, yang gaji + tunjangannya dibawah 5juta 
select concat(nama_depan,' ',nama_belakang) as nama_lengkap,
tb_jabatan.nama_jabatan,
(gaji_pokok + tunjangan_jabatan) as total_tunjangan
from tb_karyaawan 
left join tb_pekerja on tb_karyaawan.nip=tb_pekerja.nip 
right join tb_jabatan on tb_jabatan.kd_jabatan=tb_pekerja.kode_jabatan 
where (gaji_pokok+tunjangan_jabatan) < 5000000
--2.Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, 
--yg gendernya pria dan penempatan kerjanya diluar sukabumi
select  concat(nama_depan,' ',nama_belakang) as nama_lengkap,
tb_jabatan.nama_jabatan,
tb_devisi.nama_divisi,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) as total_gaji,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*0.05 as pajak,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)-((gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*0.05) as gaji_bersih
from tb_karyaawan
left join tb_pekerja on tb_karyaawan.nip=tb_pekerja.nip
right join tb_jabatan on tb_jabatan.kd_jabatan=tb_pekerja.kode_jabatan
join tb_devisi on tb_devisi.kd_divisi=tb_pekerja.kode_devisi 
where tb_karyaawan.jenis_kelamin='pria' and tb_pekerja.kota_penempatan !='sukabumi'

--3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja * 7) 
select tb_karyaawan.nip, concat(nama_depan,' ',nama_belakang) as nama_lengkap,
tb_jabatan.nama_jabatan,
tb_devisi.nama_divisi,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja*7 )*0.25 as bonus 
from tb_karyaawan
left join tb_pekerja on tb_karyaawan.nip=tb_pekerja.nip
left join tb_jabatan on tb_jabatan.kd_jabatan=tb_pekerja.kode_jabatan
left join tb_devisi on tb_devisi.kd_divisi=tb_pekerja.kode_devisi
--4. Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
select CONCAT(nama_depan,'',nama_belakang) as nama_lengkap,
tb_jabatan.nama_jabatan,
tb_devisi.nama_divisi,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) as total_gaji,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*0.05 as infak
from tb_karyaawan 
left join tb_pekerja on tb_karyaawan.nip=tb_pekerja.nip
right join tb_jabatan on tb_jabatan.kd_jabatan=tb_pekerja.kode_jabatan
left join tb_devisi on tb_devisi.kd_divisi=tb_pekerja.kode_devisi
where tb_jabatan.nama_jabatan ='manager'
--5. Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan
--total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
select CONCAT(nama_depan,'',nama_belakang) as nama_lengkap,
tb_jabatan.nama_jabatan,
tb_karyaawan.pendidikan_terakhir,
2000000 as tunjangan_pendidikan,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja+2000000) as total_gaji
from tb_karyaawan
left join tb_pekerja on tb_karyaawan.nip=tb_pekerja.nip
left join tb_jabatan on tb_jabatan.kd_jabatan=tb_pekerja.kode_jabatan where tb_karyaawan.pendidikan_terakhir like 's1%'
--6 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
 	--MGR = (bonus=25% dari total gaji*7) ,
 	--Staff = (bonus=25% dari total gaji*5) ,
 	--other = (bonus=25% dari total gaji*2) ,
select tb_karyaawan.nip,CONCAT(nama_depan,'',nama_belakang) as nama_lengkap,
tb_jabatan.nama_jabatan,
tb_devisi.nama_divisi,
case

when nama_jabatan='manager'then 
((gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*7)*0.25
when nama_jabatan='staff'then 
((gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*5)*0.25
else
((gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*2)*0.25
end bonus

from tb_karyaawan 
join tb_pekerja on tb_karyaawan.nip=tb_pekerja.nip
join tb_jabatan on tb_jabatan.kd_jabatan=tb_pekerja.kode_jabatan
join tb_devisi on tb_devisi.kd_divisi=tb_pekerja.kode_devisi

--7. Buatlah kolom nip pada table karyawan sebagai kolom unique
alter table karyaawan
add constraint nip_unique unique(nip)

--8. buatlah kolom nip pada table employee sebagai index
create index index_nip
on tb_karyaawan (nip)
--9. Tampilkan nama lengkap dan nama belakangnya  diubah menjadi huruf capital dengan last namanya di awali dengan huruf W 
select (nama_depan +' '+ upper (nama_belakang)) as nama_lengkap 
from tb_karyaawan where nama_belakang like 'w%'