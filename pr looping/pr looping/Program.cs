﻿using System;

namespace pr_looping
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Soal 1");
                Console.WriteLine("Soal 2");
                Console.WriteLine("Soal 3");
                Console.WriteLine("Soal 4");
                Console.WriteLine("Soal 5");
                Console.WriteLine("Soal 6");
                Console.WriteLine("Soal 7");
                Console.WriteLine("Soal 8");
                Console.WriteLine("Soal 9");
                Console.Write("silahkan pilih soal ?");
                string pilih = Console.ReadLine();
                switch (pilih)
                {
                    case "1":
                        Console.WriteLine("ini soal 1");
                        soal1();
                        break;
                    case "2":
                        Console.WriteLine("ini soal 2");
                        soal2();
                        break;
                    case "3":
                        Console.WriteLine("ini soal 3");
                        soal3();
                        break;
                    case "4":
                        Console.WriteLine("ini soal 4");
                        soal4();
                        break;
                    case "5":
                        Console.WriteLine("ini soal 5");
                        soal5();
                        break;
                    case "6":
                        Console.WriteLine("ini soal 6");
                        soal6();
                        break;
                    case "7":
                        Console.WriteLine("ini soal 7");
                        soal7();
                        break;
                    case "8":
                        Console.WriteLine("ini soal 8");
                        soal8();
                        break;
                    case "9":
                        Console.WriteLine("ini soal 9");
                        soal9();
                        break;
                }
                Console.WriteLine("ulangi y/n = ");
                string yn = Console.ReadLine();
                if (yn.ToLower() == "n")
                {
                    ulangi = false;

                }

            }
        }
        static void soal1()
        {
            Console.Write("masukan nilai  : ");
            int b = int.Parse(Console.ReadLine());
            for (int i = 1; i <= b; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("*");
                }
                Console.Write("\n");

            }
        }
        static void soal2()

        {
            Console.Write("masukan nilai  : ");
            int b = int.Parse(Console.ReadLine());
            for (int i = 1; i <= b; i++)
            {
                for (int j = b; j > i; j--)
                {
                    Console.Write(" ");
                }
                for (int c = 1; c <= i; c++)
                {
                    Console.Write("*");
                }
                Console.Write("\n");
            }
        }
        static void soal3()
        {
            Console.WriteLine("masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int a = 0;
            int b = 1;
            int jwb;
            for (int i = 0; i < n; i++)
            {
                jwb = b;
                Console.Write(jwb + "");
                jwb = a + b;
                a = b;
                b = jwb;
            }



        }
        static void soal4()
        {
            Console.WriteLine("masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int a = 1;
            int b = -1;
            int c = 1;
            int jwb;
            for (int i = 0; i < n; i++)
            {
                jwb = c;
                Console.Write(jwb + "");
                jwb = a + b + c;
                a = b;
                b = c;
                c = jwb;
            }

        }
        static void soal5()
        {
            Console.Write("Masukkan Batas Bilangan Prima : ");

            bool prima = true;

            int bilangan = int.Parse(Console.ReadLine());


            for (int i = 2; i < bilangan; i++)
            {
                for (int j = 2; j < i; j++)
                {
                    if ((i % j) == 0)
                    {
                        prima = false;
                        break;
                    }
                }

                if (prima)
                    Console.Write(i + " ");
                prima = true;
            }

        }
        static void soal6()
        {
            Console.Write("masukan nilai: ");
            int nilai = int.Parse(Console.ReadLine());
            while (nilai != 1)
            {
                if (nilai % 2 == 0)
                {
                    Console.WriteLine(nilai + "/2=" + nilai / 2);
                    nilai = nilai / 2;
                }
                else if (nilai % 3 == 0)
                {
                    Console.WriteLine(nilai + "/3= " + nilai / 3);
                    nilai = nilai / 3;
                }
                else if (nilai % 5 == 0)
                {
                    Console.WriteLine(nilai + "/5= " + nilai / 5);
                    nilai = nilai / 5;
                }
            }
        }
        static void soal7()
        {
            int[] angka = { 3, 2, 1, 5, 4, };
            Array.Sort(angka);
            foreach (int i in angka)
            {
                Console.Write(i);
            }
        }
        static void soal8()
        {
            Console.Write("masukan nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            int angka = 3;
            for (int i = 3; i <= nilai; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write("* ");
                    angka *= 3;
                }
                else
                {
                    Console.Write(angka + " ");
                    angka *= 3;
                }
            }
        }
        static void soal9()
        {
            Console.Write("masukan nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            int angka = -5;
            for (int i = 1; i <= nilai; i++)
            {
                if (angka<0)
                {
                    Console.Write(angka+" ");
                    angka *= -1;
                    angka += 5;
                }
                else
                {
                    Console.Write(angka + " ");
                    angka *= -1;
                    angka -= 5;
                }
            }
        }
        static void soal10()
        {

        }
        static void soal11()
        {

        }
        static void soal12()
        {

        }
    }
}
