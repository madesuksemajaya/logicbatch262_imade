use latihan2
--buat table
create table gundar(
id int primary key identity(1,1),
nim varchar(5) not null,
nama varchar(10) not null,
jenis_kelamin varchar(1)not null,
alamat varchar(100)not null
)

--buat table
create table ambil_mk(
id int primary key identity(1,1),
nim varchar(5) not null,
kode_mk varchar(7) not null
)

--buat table
create table matakuliah(
id int primary key identity(1,1),
kode_mk varchar(7) not null,
nama_mk varchar(25) not null,
sks int not null,
semester int not null
)

--input table
insert into gundar
(nim,nama,jenis_kelamin,alamat)
values
('101','arif','L','jl.kenangan'),
('102','budi','L','jl.jombang'),
('103','Wati','p','jl.surabaya'),
('104','ika','p','jl.jombang'),
('105','tono','L','jl.jakarta'),
('106','iwan','L','jl.bandung'),
('107','sari','p','jl.malang')

insert into matakuliah
(kode_mk,nama_mk,sks,semester)
values
('PTI447','Praktikum basis data',1,3),
('TIK342','Praktikum basis data',1,3),
('PTI333','basis data ter distribusi',3,5),
('TIK123','jaringan komputer',2,5),
('TIK333','sistem operasi',3,5),
('PTI123','Grafika multimedia',3,5),
('PTI777','sistem informasi',2,3)


insert into ambil_mk
(nim,kode_mk)
values
('101','PTI447'),
('103','TIK333'),
('104','PTI333'),
('104','PTI777'),
('111','PTI123'),
('123','PTI999')


--1.	Tampilkan nama mahasiswa dan matakuliah yang diambil
select nama,nama_mk from gundar join ambil_mk on gundar.nim=ambil_mk.nim 
join matakuliah on matakuliah.kode_mk=ambil_mk.kode_mk
--2.	Tampilkan data mahasiswa yang tidak mengambil matakuliah
select * from gundar left join ambil_mk on gundar.nim=ambil_mk.nim where kode_mk is null
--3.	Kelompokan data mahasiswa yang tidak mengambil matakuliah berdasarkan jenis kelaminnya, kemudian hitung banyaknya.
select count(jenis_kelamin),jenis_kelamin from gundar left join ambil_mk on gundar.nim=ambil_mk.nim 
where kode_mk is null group by jenis_kelamin  
--4.	Dapatkan nim dan nama mahasiswa yang mengambil matakuliah beserta kode_mk dan nama_mk yang diambilnya 
select nama,gundar.nim,ambil_mk.kode_mk,nama_mk from gundar join ambil_mk 
on gundar.nim=ambil_mk.nim join matakuliah on matakuliah.kode_mk=ambil_mk.kode_mk 
--5.	Dapatkan nim, nama, dan total sks yang diambil oleh mahasiswa, Dimana total sksnya lebih dari 4 dan kurang dari 10. 
select nama,m.nim,sum(sks) as total_SKS  from ambil_mk as m
join gundar as s
on s.nim=m.nim
join matakuliah as k
on m.kode_mk=k.kode_mk
group by nama,m.nim
having sum(sks)>4 and sum(sks)<10
--6.Dapatkan data matakuliah yang tidak diambil oleh mahasiswa terdaftar (mahasiswa yang terdaftar adalah mahasiswa yang tercatat di tabel mahasiswa).
select nama_mk,matakuliah.kode_mk,sks,semester from gundar join ambil_mk on gundar.nim=ambil_mk.nim
right join matakuliah on ambil_mk.kode_mk=matakuliah.kode_mk where gundar.nama is null






