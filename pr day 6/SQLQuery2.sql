create database DBPenerbit
use DBPenerbit

--table pengarang
create table tblPengarang(
id int primary key identity(1,1),
kd_pengarang varchar(7) not null,
nama varchar(30) not null,
alamat varchar(80) not null,
kota varchar(15) not null,
kelamin varchar(1) not null
)

--input data ke table pengarang

insert into tblPengarang
(kd_pengarang,nama,alamat,kota,kelamin)
values
('p0001','ashadi','jl.beo 25','yogya','p'),
('p0002','rian','jl.solo 123','yogya','p'),
('p0003','suwadi','jl.semangka 13','bandung','p'),
('p0004','siti','jl.durian 15','solo','w'),
('p0005','amir','jl.gajah 33','kudus','p'),
('p0006','suparman','jl.harimau 25','jakarta','p'),
('p0007','jaja','jl.singa 7','bandung','p'),
('p0008','saman','jl.naga 12','yogya','p'),
('p0009','anwar','jl.tidar 6a','magelang','p'),
('p0010','fatmawati','jl.renjana 4','bogor','w')

--membuat table gaji
create table tblgaji(
id int primary key identity(1,1),
kd_pengarang varchar(7) not null,
nama varchar(30) not null,
gaji decimal(18,4) not null
)

--input nilai gaji
insert into tblgaji
(kd_pengarang,nama,gaji)
values
('p0002','rian',600000),
('p0005','amir',700000),
('p0004','siti',500000),
('p0003','suwadi',1000000),
('p0010','fatmawati',600000),
('p0008','saman',750000)

--1.hitung dan tampilkan jumlah pengarang dari table pengarang
--memanggil table nama dan jumlah pengarang
select nama,count(*)jml_pengarang from tblPengarang group by nama
--menghitung jumlah pengarang 1 kolum
select count(nama) from tblpengarang
--2.hitung jumlah pengarang wanita dan peria
select kelamin, count(*) as jml_kelamin from tblPengarang group by kelamin
--3.tampilkan record kota dan jumlah kota dari table pengarang
select kota, count(*) as jml_kota from tblPengarang group by kota
--4.menampilkan kota yang lebih dari 1
select kota, count(*) as jml_kota from tblPengarang group by kota
having count(*)!=1
--5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table
select * from tblPengarang order by kd_pengarang desc 
--6.    Tampilkan gaji tertinggi dan terendah.
select * from tblgaji order by gaji desc
--7.    Tampilkan gaji diatas 600.000.
select * from tblgaji where gaji >600000
--8.    Tampilkan jumlah gaji.
select sum(gaji) as jml_gaji from tblgaji
--9.    Tampilkan jumlah gaji berdasarkan Kota
select kota,count(kota),sum(gaji) from tblPengarang as pengarang join tblgaji as gaji on 
pengarang.kd_pengarang=gaji.kd_pengarang group by kota 
--10.    Tampilkan seluruh record pengarang antara P0001-P0006 dari tabel pengarang.
select * from tblPengarang where kd_pengarang between 'p0001' and 'p0006'
--11.    Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
select * from tblpengarang where kota='yogya'or kota='solo'or kota='magelang'
--12.    Tampilkan seluruh data yang bukan yogya dari tabel pengarang
select * from tblPengarang where kota!= 'yogya'
--13.    Tampilkan seluruh data pengarang yang nama (dapat digabungkan atau terpisah):
        --a.    dimulai dengan huruf [A]
		select * from tblPengarang where nama LIKE 'a%' 
        --b.    berakhiran [i]
		
		select * from tblPengarang where nama LIKE '%i'
        --c.    huruf ketiganya [a]
		
		select * from tblPengarang where nama LIKE '__a%'
        --d.    tidak berakhiran [n]
		
		select * from tblPengarang where nama NOT LIKE '%n'
--14.    Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
select * from tblPengarang join tblgaji on tblPengarang.kd_pengarang=tblgaji.kd_pengarang  
--15.    Tampilan kota yang memiliki gaji dibawah 1.000.000
select * from tblPengarang join tblgaji on 
tblPengarang.kd_pengarang=tblgaji.kd_pengarang where gaji<1000000
--16.    Ubah panjang dari tipe kelamin menjadi 10
alter table tblPengarang alter column kelamin varchar(10)
--17.    Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang
alter table tblPengarang add gelar varchar(12)
--18.    Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru
Update tblPengarang set alamat='Jl. Cendrawasih 65',kota='Pekanbaru' where nama='rian' 
--19.	Buatlah view untuk attribute Kd_Pengarang, Nama, Kota dengan nama vwPengarang
create view vwpengarang as select kd_pengarang,nama,kota from tblPengarang 
























