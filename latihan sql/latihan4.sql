create database film
use film

create table artis(
kd_artis varchar(100) primary key not null,
nm_artis varchar(500) not null,
jk varchar (100) not null,
bayaran int,
award int,
negara varchar (100) not null
)

select * from artis

create table negara(
kd_negara varchar(100) primary key not null,
nm_negara varchar(100)not null
)

create table genre(
kd_genre varchar(50) primary key not null,
nm_genre varchar(50) not null
)

create table produser(
kd_produser varchar (50) primary key not null,
nm_produser varchar (50) not null,
international varchar(50) 
)

create table film(
kd_film varchar(10) primary key not null,
nm_film varchar(55)not null,
genre varchar(55)not null,
artis varchar(55)not null,
produser varchar(55) not null,
pendapatan decimal(18,2),
nominasi int
)

insert into artis
(kd_artis,nm_artis,jk,bayaran,award,negara)
values
('A001','ROBERT DOWNEY JR','PRIA',2000000000,2,'AS'),
('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

insert into negara
(kd_negara,nm_negara)
values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

insert into genre
(kd_genre,nm_genre)
values
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

insert into produser
(kd_produser,nm_produser,international)
VALUES
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','NO'),
('PD04','PARKIT','NO'),
('PD05','PARAMOUNT PICTURE','YA')
--select * from produser
insert into film
(kd_film,nm_film,genre,artis,produser,pendapatan,nominasi)
VALUES
('F001','IRON MAN ','G001','A001','PD01',2000000000,3),
('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
('F004','AVENGER CIVIL WAR','G001','A001','PD01',2000000000,1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('F006','THE RAID','G001','A004','PD03',800000000,5),
('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
('F009','POLICE STORY','G001','A003','PD02',700000000,3),
('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
('F013','KUNGFU PANDA','G003','A003','PD05',923000000,5)
drop table film
--1. Menampilkan nama film dan nominasi dari nominasi yang paling besar
select nm_film,nominasi from film
order by nominasi desc
--2. Menampilkan nama film dan nominasi yang paling banyak memperoleh nominasi
select nm_film,nominasi from film 
where nominasi in(select max(nominasi) from film)
--3. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
select nm_film,nominasi from film 
where nominasi in(select min(nominasi) from film) 
--4. Menampilkan nama film dan pendapatan dari yang paling kecil
select nm_film,pendapatan from film 
where pendapatan in(select min(pendapatan) from film)
--5. Menampilkan nama film dan pendapatan yang terbesar
select nm_film,pendapatan from film 
where pendapatan in(select max(pendapatan) from film)
--6. Menampilkan nama film yang huruf depannya 'p'
 select nm_film from film 
 where nm_film like 'p%'
--7. Menampilkan nama film yang huruf terakhir 'y'
select nm_film from film 
 where nm_film like '%y' 
--8. Menampilkan nama film yang mengandung huruf 'd'
select nm_film from film 
 where nm_film like '%d%' 
--9. Menampilkan nama film dengan pendapatan terbesar mengandung huruf 'o'
select nm_film,pendapatan from film
where pendapatan in(select max(pendapatan)  from film) and nm_film like '%o%'
--10. Menampilkan nama film dengan pendapatan terkecil mengandung huruf 'o' 
select nm_film,pendapatan from film
where pendapatan in(select min(pendapatan)  from film) and nm_film like '%o%'
--11. Menampilkan nama film dan artis
select nm_film,nm_artis 
from film
join artis on film.artis=artis.kd_artis 
--12. Menampilkan nama film yang artisnya berasal dari negara hongkong
select nm_film,nm_artis,nm_negara 
from film
right join artis on film.artis=artis.kd_artis 
left join negara on artis.negara=negara.kd_negara
where negara.nm_negara='Hongkong' 
--13. Menampilkan nama film yang artisnya bukan berasal dari negara yang tidak mengandung huruf 'o'
select nm_film,nm_artis,nm_negara 
from film
right join artis on film.artis=artis.kd_artis 
left join negara on artis.negara=negara.kd_negara
where negara.nm_negara not like '%o%'
--14. Menampilkan nama artis yang tidak pernah bermain film
select nm_artis 
from film
right join artis on film.artis=artis.kd_artis 
left join negara on artis.negara=negara.kd_negara
where film.nm_film is null 
--15. Menampilkan nama artis yang bermain film dengan genre drama
select nm_artis
from artis
left join film on artis.kd_artis=film.artis
left join genre on genre.kd_genre=film.genre
where genre.nm_genre='DRAMA'
--16. Menampilkan nama artis yang bermain film dengan genre Action
select nm_artis
from artis
left join film on artis.kd_artis=film.artis
left join genre on genre.kd_genre=film.genre 
where genre.nm_genre='ACTION'
group by artis.nm_artis
--17. Menampilkan data negara dengan jumlah filmnya
select nm_negara,count(nm_film) as jumlah_film  from negara
left join artis on negara.kd_negara=artis.negara
join film on artis.kd_artis=film.artis
group by negara.nm_negara
--18. Menampilkan nama film yang skala internasional
select nm_film,international from film 
join produser on produser.kd_produser=film.produser
where international='ya'
--19. Menampilkan jumlah film dari masing2 produser 
select nm_produser, count(nm_film) as jumlah_film from produser
left join film on produser.kd_produser=film.produser
group by nm_produser
--20. Menampilkan jumlah pendapatan produser marvel secara keseluruhan
select sum(pendapatan) as jumlah_pendapatan,
nm_produser 
from film 
join produser on produser.kd_produser=film.produser
GROUP BY nm_produser
having nm_produser='MARVEL'

 
