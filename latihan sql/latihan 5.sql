create database karyawan
use karyawan

create table karyawan(
id bigint primary key identity(1,1)not null,
nomor_induk varchar(7) not null,
nama varchar(30) not null,
alamat text NOT NULL,
tanggal_lahir date NOT NULL,
tanggal_masuk date NOT NULL,
)

create table cuti_karyawan(
id  bigint PRIMARY KEY  identity(1,1)not null,
nomor_induk varchar (7) not null,
tanggal_mulai date not null,
lama_cuti int not null,
keterangan text NOT NULL
)
drop table cuti_karyawan

insert into karyawan
(nomor_induk,nama,alamat,tanggal_lahir,tanggal_masuk)
values
('IP06001','AGUS','JLN.GAJAH MADA 115A,JAKARTA PUSAT','8/1/70','7/7/06'),
('IP06002','AMIN','JLN.BUNGUR SARI V NO,178,BANDUNG','5/3/77','7/6/06'),
('IP06003','YUSUF','JLN.YOSODPURO 15,SURABAYA','8/9/73','7/8/06'),
('IP07004','ALYSSA','JLN.CENDANA NO.6 BANDUNG','2/14/83','1/5/07'),
('IP07005','MAULANA','JLN.AMPERA RAYA NO.1','10/10/85','2/5/07'),
('IP07006','AFIKA','JLN.PEJATEN BARAT NO.6A','3/9/87','6/9/07'),
('IP07007','JAMES','JLN.PADJADJARAN NO.111,BANDUNG','5/19/88','6/9/06'),
('IP09008','OCTAVANUS','JLN.GAJAH MADA 101, SEMARANG','10/7/88','8/8/08'),
('IP09009','NUGROHO','JLN.DUREN TIGA 196,JAKARTA SELATAN','1/20/88','11/11/08'),
('IP09010','RAISA','JLN.NANGKA, JAKARTA SELATAN','12/29/89','2/9/09')

insert into cuti_karyawan
(nomor_induk,tanggal_mulai,lama_cuti,keterangan)
values
('IP06001','2/1/12',3,'ACARA KELUAR'),
('IP06001','2/13/12',4,'ANAK SAKIT'),
('IP07007','2/15/12',2,'NENEK SAKIT'),
('IP06003','2/17/12',1,'MENDAFTAR SEKOLAH ANAK'),
('IP07006','2/20/12',5,'MENIKAH'),
('IP07004','2/27/12',1,'IMUNISASI ANAK')
--Tgl sekarang 2/16/12
--1.	Menampilkan 3 karyawan yang pertama kali masuk.
SELECT top 3 tanggal_masuk,nama from karyawan
order by tanggal_masuk
--2.	Menampilkan daftar karyawan yang saat ini sedang cuti. Daftar berisi nomor_induk, nama, tanggal_mulai, lama_cuti dan keterangan.
select karyawan.nomor_induk,nama,DATEADD(day,lama_cuti,tanggal_mulai)as tanggal_cuti,keterangan 
from karyawan
right join cuti_karyawan on cuti_karyawan.nomor_induk=karyawan.nomor_induk
where DATEADD(day, lama_cuti, tanggal_mulai)>'2/16/12' and tanggal_mulai <='2/16/12'
--3.	Menampilkan daftar karyawan yang sudah cuti lebih dari satu hari. Daftar berisi no_induk, nama, jumlah (berapa hari cuti).
select karyawan.nomor_induk,nama,sum(lama_cuti) as jumlah_cuti from karyawan
join cuti_karyawan on cuti_karyawan.nomor_induk=karyawan.nomor_induk 
group by karyawan.nomor_induk,karyawan.nama
having sum(lama_cuti) >1
--4.	Menampilkan sisa cuti tiap karyawan tahun ini, jika di ketahui jatah cuti setiap karyawan tahun ini adalah 12. Daftar berisi no_induk, nama, sisa_cuti
select karyawan.nomor_induk,nama,12-sum(isnull(lama_cuti,0)) as sisa_cuti 
from karyawan
left join cuti_karyawan on cuti_karyawan.nomor_induk=karyawan.nomor_induk
group by karyawan.nomor_induk,karyawan.nama

