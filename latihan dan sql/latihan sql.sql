CREATE DATABASE DB_Universitas 

USE DB_Universitas
CREATE TABLE JurusanKuliah(
ID INT primary key identity (1,1) NOT NULL,
Kode_Jurusan varchar (12) NOT NULL,
Deskripsi varchar (50) NOT NULL,
)


ALTER TABLE JurusanKuliah ALTER COLUMN Deskripsi varchar(50)

INSERT INTO JurusanKuliah(Kode_Jurusan, Deskripsi) VALUES
('KA001','Teknik Informatika'),
('KA002','Management Bisnis'),
('KA003','Ilmu Komunikasi'),
('KA004','Sastra Inggris'),
('KA005','Ilmu Pengetahuan Alam Dan Matematika'),
('KA006','Kedokteran')

SELECT *FROM JurusanKuliah

CREATE TABLE Dosen(
ID int primary key identity (1,1) NOT NULL,
Kode_Dosen varchar (14) NOT NULL,
Nama_Dosen varchar (15) NOT NULL,
Kode_Matakuliah varchar (12) NOT NULL,
Kode_Fakultas varchar (13) NOT NULL,
)

DROP TABLE Dosen

INSERT INTO Dosen(Kode_Dosen, Nama_Dosen, Kode_Matakuliah, Kode_Fakultas) VALUES
('GK001', 'Ahmad Prasetyo', 'KPK001', 'TK002'),
('GK002', 'Hadi Fuladi', 'KPK002', 'TK001'),
('GK003', 'Johan Goerge', 'KPK003', 'TK002'),
('GK004', 'Bima Darmawan', 'KPK004', 'TK002'),
('GK005', 'Gatot Wahyudi', 'KPK005', 'TK001')

DELETE FROM Dosen
SELECT *FROM Dosen
CREATE TABLE MataKuliah(
ID int primary key identity (1,1) NOT NULL,
Kode_Mata_kuliah varchar (12) NOT NULL,
Nama_Matakuliah varchar (50) NOT NULL,
Keaktifan_Status varchar (15) NOT NULL,
)

DROP TABLE MataKuliah
ALTER TABLE MataKuliah ALTER COLUMN Nama_Matakuliah varchar(50)

INSERT INTO MataKuliah(Kode_Mata_kuliah, Nama_Matakuliah, Keaktifan_Status) VALUES
('KPK001','Algoritma Dasar','Aktif'),
('KPK002','Basis Data','Aktif'),
('KPK003','Kalkulus','Aktif'),
('KPK004','Pengantar Bisnis','Aktif'),
('KPK005','Matematika Ekonomi & Bisnis','Non Aktif')

DELETE FROM MataKuliah
SELECT *FROM MataKuliah

CREATE TABLE Mahasiswa(
ID int primary key identity (1,1) NOT NULL,
Kode_Mahasiswa varchar (12) NOT NULL,
Nama_Mahasiswa varchar (20) NOT NULL,
Alamat_Mahasiswa varchar (30) NOT NULL,
Kode_Jurusan varchar (12) NOT NULL,
Kode_Matkul varchar (12) NOT NULL,
)

DROP TABLE Mahasiswa


INSERT INTO Mahasiswa(Kode_Mahasiswa, Nama_Mahasiswa, Alamat_Mahasiswa, Kode_Jurusan, Kode_Matkul) VALUES
('MK001','Fullan','Jl. Hati Besar No.63 RT 13','KA001','KPK001'),
('MK002','Fullan Binharjo','Jl.Biak Kebagusan No.34','KA002','KPK002'),
('MK003','Sardine Himura','Jl.Kebajian No.84','KA001','KPK003'),
('MK004','Isani Isabul','Jl.Merak Merpati No.78','KA001','KPK001'),
('MK005','Charlie Birawa','Jl.Air Terjun No.56','KA003','KPK002')

DELETE FROM Mahasiswa
SELECT *FROM Mahasiswa

CREATE TABLE Nilai_Mahasiswa(
ID int primary key identity (1,1) NOT NULL,
Kode_Nilai varchar (12) NOT NULL,
Kode_Mhs varchar (12) NOT NULL,
Kode_Organisasi varchar (9) NOT NULL,
Nilai int NOT NULL,
)

DROP TABLE Nilai_Mahasiswa

INSERT INTO Nilai_Mahasiswa (Kode_Nilai, Kode_Mhs, Kode_Organisasi, Nilai) VALUES
('SK001','MK004','KK001','90'),
('SK002','MK001','KK001','80'),
('SK003','MK002','KK003','85'),
('SK004','MK004','KK002','95'),
('SK005','MK005','KK005','70')

DELETE FROM Nilai_Mahasiswa

CREATE TABLE Fakultas(
ID bigint primary key identity (1,1),
Kode_Fakultas varchar (13) NOT NULL,
Penjelasan varchar (30) NOT NULL,
)

INSERT INTO Fakultas(Kode_Fakultas, Penjelasan) VALUES
('TK001','Teknik Informatika'),
('TK002','Matematika'),
('TK003','Sistem Informatika')

CREATE TABLE Organisasi(
ID bigint primary key identity (1,1),
Kode_Organisasi varchar (12) NOT NULL,
Nama_Organisasi varchar (17) NOT NULL,
Status_Organisasi varchar (13) NOT NULL,
)

ALTER TABLE Organisasi ALTER COLUMN Nama_Organisasi varchar(50)


INSERT INTO Organisasi(Kode_Organisasi, Nama_Organisasi, Status_Organisasi) VALUES
('KK001','Unit Kegiatan Mahasiswa (UKM)','Aktif'),
('KK002','Badan Eksekutif Mahasiswa Fakultas (BEMF)','Aktif'),
('KK003','Dewan Perwakilan Mahasiswa Universitas (DPMU)','Aktif'),
('KK004','Badan Eksekutif Mahasiswa Universitas (BEMU)','Non Aktif'),
('KK005','Himpunan Mahasiswa Jurusan','Non Aktif'),
('KK006','Himpunan Kompetisi Jurusan','Aktif')


SELECT *FROM JurusanKuliah
SELECT *FROM Dosen
SELECT *FROM MataKuliah
SELECT *FROM Mahasiswa
SELECT *FROM Nilai_Mahasiswa
SELECT *FROM Fakultas
SELECT *FROM Organisasi


-- NOMER 2 Ubah panjang karakter nama dosen menjadi 200
ALTER TABLE Dosen ALTER COLUMN Nama_Dosen VARCHAR (200)

-- NOMER 3 tampilkan kode mahasiswa, nama mahasiswa, nama MataKuliah, deskripsi Jurusan Kuliah dengan kode mahasiswa  mk001
SELECT Kode_Mahasiswa, Nama_Mahasiswa, Nama_Matakuliah, Deskripsi FROM Mahasiswa
LEFT JOIN MataKuliah ON Mahasiswa.Kode_Matkul=MataKuliah.Kode_Mata_kuliah
LEFT JOIN JurusanKuliah ON JurusanKuliah.Kode_Jurusan = Mahasiswa.Kode_Jurusan
WHERE Kode_Mahasiswa = 'MK001'

-- NOMER 4 tampilkan nama Mata Kuliah, status Mata Kuliah, kode Mahasiswa, nama Mahasiswa, alamat Mahasiswa, kode Jurusan, kode Mata Kuliah dengan status Mata Kuliah tidak aktif
SELECT Nama_Matakuliah, Keaktifan_Status, Kode_Mahasiswa, Nama_Mahasiswa, Alamat_Mahasiswa, Kode_Jurusan, Kode_Mata_kuliah
FROM MataKuliah
FULL OUTER JOIN Mahasiswa ON MataKuliah.Kode_Mata_kuliah = Mahasiswa.Kode_Matkul
WHERE Keaktifan_Status = 'Non Aktif'

-- NOMER 5 tampilakn kode mahasiswa, nama mahasiswa, alamat mahasiswa dengan status organisasi aktif dan nilai diatas 79
SELECT Mahasiswa.Kode_Mahasiswa, Nama_Mahasiswa, Alamat_Mahasiswa, Status_Organisasi, Nilai FROM Mahasiswa
LEFT JOIN Nilai_Mahasiswa ON Mahasiswa.Kode_Mahasiswa = Nilai_Mahasiswa.Kode_Mhs
LEFT JOIN Organisasi ON Organisasi.Kode_Organisasi = Nilai_Mahasiswa.Kode_Organisasi
WHERE Status_Organisasi = 'Aktif' AND Nilai >=79

-- NOMER 6 tampilkan kode Mata Kuliah, nama Mata Kuliah, status Mata Kuliah dengan nama Matakuliah mengandung huruf �n�
SELECT Kode_Mata_kuliah, Nama_Matakuliah, Keaktifan_Status FROM MataKuliah
WHERE Nama_Matakuliah LIKE '%n%'

-- NOMER 7 tampilkan  nama mahasiswa yang memiliki organisasi lebih dari 1
SELECT COUNT (Nama_Mahasiswa) AS Jum_Organisasi, Nama_Mahasiswa FROM Mahasiswa
LEFT JOIN Nilai_Mahasiswa ON Mahasiswa.Kode_Mahasiswa = Nilai_Mahasiswa.Kode_Mhs
LEFT JOIN Organisasi ON Organisasi.Kode_Organisasi = NIlai_Mahasiswa.Kode_Organisasi
GROUP BY Nama_Mahasiswa
HAVING COUNT(Nama_Mahasiswa)>1

-- NOMER 8 tampilkan kode mahasiswa, nama mahasiswa, nama mata kuliah, deskripsi jurusan, nama dosen, status mata kuliah, deskripsi fakultas dengan kode mahasiswa mk001
SELECT Kode_Mahasiswa, Nama_Mahasiswa, Nama_Matakuliah,JurusanKuliah.Deskripsi 
Nama_Dosen, MataKuliah.Keaktifan_Status, Fakultas.Penjelasan AS Fakultas FROM Mahasiswa
LEFT JOIN Dosen ON Dosen.Kode_Matakuliah=Mahasiswa.Kode_Matkul
LEFT JOIN MataKuliah ON Mahasiswa.Kode_Matkul = MataKuliah.Kode_Mata_kuliah
LEFT JOIN JurusanKuliah ON JurusanKuliah.Kode_Jurusan=Mahasiswa.Kode_Jurusan
LEFT JOIN Fakultas ON Fakultas.Kode_Fakultas=Dosen.Kode_Fakultas
WHERE Kode_Mahasiswa = 'MK001'

-- NOMER 9 buat view dengan kode mahasiswa, nama mahasiswa, nama mata kuliah, nama dosen, status matakuliah, deskripsi fakultas 
CREATE VIEW ViewMahasiswa
AS SELECT Kode_Mahasiswa, Nama_Mahasiswa, Nama_Matakuliah, JurusanKuliah.Deskripsi,
Nama_Dosen, MataKuliah.Keaktifan_Status, Fakultas.Penjelasan AS Fakultas FROM Mahasiswa
LEFT JOIN Dosen ON Dosen.Kode_Matakuliah=Mahasiswa.Kode_Matkul
LEFT JOIN MataKuliah ON Mahasiswa.Kode_Matkul = MataKuliah.Kode_Mata_kuliah
LEFT JOIN JurusanKuliah ON JurusanKuliah.Kode_Jurusan=Mahasiswa.Kode_Jurusan
LEFT JOIN Fakultas ON Fakultas.Kode_Fakultas=Dosen.Kode_Fakultas
SELECT *FROM ViewMahasiswa


-- NOMER 10 tampilkan seluruh data dari virtual table dimana kode mahasiswa MK001
SELECT *FROM ViewMahasiswa
WHERE Kode_Mahasiswa = 'MK001'