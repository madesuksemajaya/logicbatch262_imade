﻿using System;

namespace logika
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("soal 1");
            Console.WriteLine("soal 2");
            Console.WriteLine("soal 3");
            Console.WriteLine("soal 4");
            Console.WriteLine("soal 5");
            Console.WriteLine("soal 6");
            Console.WriteLine("soal 7");
            Console.WriteLine("soal 8");
            Console.WriteLine("soal 9");
            Console.WriteLine("soal 10");
            Console.Write("silahkan pilih soal ? ");
            string pilih = Console.ReadLine();

            switch (pilih)
            {
                case "1":
                    Console.WriteLine("ini soal 1");
                    soal1();
                    break;
                case "2":
                    Console.WriteLine("ini soal 2");
                    soal2();
                    break;
                case "3":
                    Console.WriteLine("ini soal 3");
                    soal3();
                    break;
                case "4":
                    Console.WriteLine("ini soal 4");
                    soal4();
                    break;
                case "5":
                    Console.WriteLine("ini soal 5");
                    soal5();
                    break;
                case "6":
                    Console.WriteLine("ini soal 6");
                    soal6();
                    break;
                case "7":
                    Console.WriteLine("ini soal 7");
                    soal7();
                    break;
                case "8":
                    Console.WriteLine("ini soal 8");
                    soal8();
                    break;
                case "9":
                    Console.WriteLine("ini soal 9");
                    soal9();
                    break;
                case "10":
                    Nilai10 n1 = new Nilai10();
                    n1.nilai10();
                    break;
            }
        }
        static void soal1()
        {
            //memberi nilai
            Console.Write("masukan Nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            //kondisi
            if (nilai >= 85)

            {
                Console.WriteLine("kamu mendapatkan nilai A");
                Console.WriteLine("A");
            }
            else if (nilai >= 70)
            {
                Console.WriteLine("kamu mendapatkan nilai B");
                Console.WriteLine("B");
            }
            else if (nilai >= 55)
            {
                Console.WriteLine("kamu mendapatkan nilai C");
                Console.WriteLine("C");
            }
            else
            {
                Console.WriteLine("kamu mendapatkan nilai D");
                Console.WriteLine("D");
            }
        }
        static void soal2()
        {
            Console.Write("pembelian pulsa : ");
            int pulsa = int.Parse(Console.ReadLine());
            if (pulsa >= 50000)

            {
                Console.WriteLine("pulsa : " + pulsa);
                Console.WriteLine("point : 400");
            }
            else if (pulsa >= 25000)
            {
                Console.WriteLine("pulsa : " + pulsa);
                Console.WriteLine("point : 200");
            }
            else if (pulsa >= 10000)
            {
                Console.WriteLine("pulsa : " + pulsa);
                Console.WriteLine("point : 80");
            }


        }
        static void soal3()
        {
            Console.Write("Masukan kode promo  : ");
            string kode = (Console.ReadLine());
            Console.Write("Belanja : ");
            int Belanja = int.Parse(Console.ReadLine());
            Console.Write("jarak  : ");
            int km = int.Parse(Console.ReadLine());
            int ongkir = 5000;
            double diskon = 0;
            double hdiskon = 0;
            double t_belanja = 0;

            if (Belanja >= 30000 && kode.ToLower() == "jktovo")

            {

                if (Belanja >= 30000)
                {
                    hdiskon = (Belanja * 0.4);
                    diskon = hdiskon;
                }
                if (km > 5)
                {
                    km = (km - 5) * 1000;
                    ongkir = (ongkir + km);
                }
                t_belanja = (Belanja - diskon + ongkir);
                Console.WriteLine("belanja : " + Belanja);
                Console.WriteLine("diskon : " + diskon);
                Console.WriteLine("ongkir : " + ongkir);
                Console.WriteLine("Total belanja : " + t_belanja);

            }
            else
            {
                Console.Write("tidak mendapatkan diskon");
            }
        }
        static void soal4()
        {
            {
                Console.Write("Belanja : ");
                int belanja = int.Parse(Console.ReadLine());
                Console.Write("ongkos kirim : ");
                int ongkosKirim = int.Parse(Console.ReadLine());
                Console.Write("Pilih Voucher : ");
                string pilihan = Console.ReadLine();
                int diskonKirim = 0;
                int diskonBelanja = 0;
                int totalBelanja = 0;

                switch (pilihan)
                {
                    case "1":
                        if (belanja >= 30000)
                        {
                            Console.WriteLine($"Belanja : {belanja}");
                            Console.WriteLine($"Ongkos Kirim : {ongkosKirim}");
                            diskonKirim = 5000;
                            Console.WriteLine($"Diskon Kirim : {diskonKirim}");
                            diskonBelanja = 5000;
                            Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                            Console.WriteLine($"Total Belanja : {(belanja - diskonBelanja) + (ongkosKirim - diskonKirim)}");

                        }
                        else
                        {
                            Console.WriteLine("kupon tidak dapat digunakan");
                        }
                        break;

                    case "2":
                        if (belanja >= 50000)
                        {
                            Console.WriteLine($"Belanja : {belanja}");
                            Console.WriteLine($"Ongkos Kirim : {ongkosKirim}");
                            diskonKirim = 10000;
                            Console.WriteLine($"Diskon Kirim : {diskonKirim}");
                            diskonBelanja = 10000;
                            Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                            Console.WriteLine($"Total Belanja : {(belanja - diskonBelanja) + (ongkosKirim - diskonKirim)}");

                        }
                        else
                        {
                            Console.WriteLine("kupon tidak dapat digunakan");
                        }
                        break;


                    case "3":
                        if (belanja >= 100000)
                        {
                            Console.WriteLine($"Belanja : {belanja}");
                            Console.WriteLine($"Ongkos Kirim : {ongkosKirim}");
                            diskonKirim = 20000;
                            Console.WriteLine($"Diskon Kirim : {diskonKirim}");
                            diskonBelanja = 20000;
                            Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                            Console.WriteLine($"Total Belanja : {(belanja - diskonBelanja) + (ongkosKirim - diskonKirim)}");

                        }
                        else
                        {
                            Console.WriteLine("kupon tidak dapat digunakan");
                        }
                        break;

                    default:
                        Console.WriteLine("Input bermasalah");
                        break;
                }
            }
        }
        static void soal5()
        {
            Console.WriteLine("Masukkan nama anda 			    : ");
            string nama = Console.ReadLine();
            Console.WriteLine("Masukkan tahun berapa anda lahir : ");
            int tahun = int.Parse(Console.ReadLine());

            if (tahun >= 1944 && tahun <= 1964)
            {
                Console.WriteLine(nama + " Berdasarkan tahun lahir anda anda tergolong Baby Boomer");
            }
            else if (tahun >= 1965 && tahun <= 1979)
            {
                Console.WriteLine(nama + " Berdasarkan tahun lahir anda anda tergolong Generation X ");
            }
            else if (tahun >= 1980 && tahun <= 1994)
            {
                Console.WriteLine(nama + " Berdasarkan tahun lahir anda anda tergolong Generation Y");
            }
            else if (tahun >= 1995 && tahun <= 2015)
            {
                Console.WriteLine(nama + " Berdasarkan tahun lahir anda anda tergolong Generation Z");
            }
            else
            {
                Console.WriteLine(nama + " tahun anda tidak ada dalam golongan yang ada");
            }
        }
        static void soal6()
        {
            Console.Write("Masukkan berat badan anda : ");
            float berat = float.Parse(Console.ReadLine());
            Console.Write("Masukkan tinggi badan anda : ");
            float tinggi = float.Parse(Console.ReadLine());
            float tinggim = tinggi / 100;
            float total = berat / (tinggim * tinggim);
            if (total <= 18.5)
            {
                Console.WriteLine("nilai bmi anda : " + total);
                Console.WriteLine("anda termasuk kurus  ");
            }
            else if (total > 25)
            {
                Console.WriteLine("nilai bmi anda : " + total);
                Console.WriteLine("anda termasuk gemuk  ");
            }
            else
            {

                Console.WriteLine("nilai bmi anda : " + total);
                Console.WriteLine("anda termasuk ideal ");

            }
        }
        static void soal7()
        {
            Console.WriteLine("putung rokok ");
            int putung = int.Parse(Console.ReadLine());
            int rokok = putung / 8;
            int sisaputung = putung % 8;
            int harga = rokok * 500;

            Console.WriteLine("batang yang berhasil dirangkai : " + rokok);
            Console.WriteLine("sisa puntung : " + sisaputung);
            Console.WriteLine("total harga rokok : " + harga);

        }
        static void soal8()
        {
            Console.Write("masukan Nilai MTK : ");
            int mtk = int.Parse(Console.ReadLine());
            Console.Write("masukan Nilai fisika : ");
            int fisika = int.Parse(Console.ReadLine());
            Console.Write("masukan Nilai kimia : ");
            int kimia = int.Parse(Console.ReadLine());
            //mencari nilai rata
            int hasil = (mtk + fisika + kimia) / 3;
            Console.WriteLine("nilai rata-rata : " + hasil);
            //kondisi
            if (hasil >= 50)

            {
                Console.WriteLine("kamu berhasil");
                Console.WriteLine("kamu hebat");
            }
            else if (hasil <= 49)
            {
                Console.WriteLine("maaf");
                Console.WriteLine("kamu gagal");

            }
        }
        static void soal9()
        {

            Console.Write("Golongan ");
            string pilih = Console.ReadLine();
            Console.WriteLine("jam kerja : ");
            int jam = int.Parse(Console.ReadLine());
            int lembur = jam - 40;
            int upah;
            int upLembur;
            int upl;
            int upkerja;
            int total;
            switch (pilih)
            {
                case "1":
                    Console.WriteLine("golang 1 ");
                    upah = 2000;
                    upkerja = 40 * upah;
                    upl = upah + (upah / 2);
                    upLembur = lembur * upl;
                    total = upkerja + upLembur;
                    Console.WriteLine("Upah : " + upkerja);
                    Console.WriteLine("Lembur : " +upLembur);
                    Console.WriteLine("Total : " + total);
                    break;
                case "2":
                    Console.WriteLine("golang 2 ");
                    upah = 3000;
                    upkerja = 40 * upah;
                    upl = upah + (upah / 2);
                    upLembur = lembur * upl;
                    total = upkerja + upLembur;
                    Console.WriteLine("Upah : " + upkerja);
                    Console.WriteLine("Lembur : " + upLembur);
                    Console.WriteLine("Total : " + total);
                    break;
                case "3":
                    Console.WriteLine("golang 3");
                    upah = 4000;
                    upkerja = 40 * upah;
                    upl = upah + (upah / 2);
                    upLembur = lembur * upl;
                    total = upkerja + upLembur;
                    Console.WriteLine("Upah : " + upkerja);
                    Console.WriteLine("Lembur : " + upLembur);
                    Console.WriteLine("Total : " + total);
                    break;
                case "4":
                    Console.WriteLine("golang 4 ");
                    upah = 5000;
                    upkerja = 40 * upah;
                    upl = upah + (upah / 2);
                    upLembur = lembur * upl;
                    total = upkerja + upLembur;
                    Console.WriteLine("Upah : " + upkerja);
                    Console.WriteLine("Lembur : " + upLembur);
                    Console.WriteLine("Total : " + total);
                    break;
            }
        }
    }
}
