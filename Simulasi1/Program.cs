﻿using System;

namespace Simulasi
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulang = true;
            while (ulang)
            {
                Console.WriteLine("pilih soal 1-11");
                Console.Write("pilih kode soal : ");
                int pilihan = int.Parse(Console.ReadLine());

                switch (pilihan)
                {
                    case 1:
                        soal1();
                        break;
                    case 2:
                        soal2();
                        break;
                    case 3:
                        soal3();
                        break;
                    case 4:
                        soal4();
                        break;
                    case 5:
                        soal5();
                        break;
                    case 6:
                        soal6();
                        break;
                    case 7:
                        soal7();
                        break;
                    case 8:
                        soal8();
                        break;
                    case 9:
                        soal9();
                        break;
                    case 10:
                        soal10();
                        break;
                    case 11:
                        soal11();
                        break;
                    default:
                        ulang = true;
                        break;

                }

                Console.Write("\napakah anda ingin mengulang (y/n)? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulang = false;
                }
                Console.Clear();
            }
        }
        static void soal1()
        {
            Console.WriteLine("masukkan input: ");
            string input = Console.ReadLine();
            char[] arrInput = input.ToCharArray();
            int cUpper = 0;


            for (int i = 0; i < arrInput.Length; i++)
            {

                if (char.IsUpper(arrInput[i]))
                {
                    cUpper++;
                }

            }
            Console.WriteLine(cUpper);
        }
        static void soal2()
        {
            Console.WriteLine("masukkan input: ");
            int input = int.Parse(Console.ReadLine());
            int x = 0;
            int u = 0;
            int j = 100;
            int hasil = 0;
            int d = 0;
            int hasil1 = 0;
            int q = 0;

            for (int i = 0; i < input; i++)
            {

                string k = j.ToString();
                char[] chark = k.ToCharArray();
                int[] intInput = Array.ConvertAll(chark, c => (int)Char.GetNumericValue(c));
                //Console.WriteLine(chark);
                for (int z = 0; z < chark.Length; z++)
                {

                    u = intInput[z] * intInput[z];
                    hasil += u;
                ulangan:
                    hasil += hasil1;
                    string h = hasil.ToString();
                    char[] p = h.ToCharArray();
                    int[] intP = Array.ConvertAll(p, c => (int)Char.GetNumericValue(c));
                    if (intP.Length > 1)
                    {
                        for (int m = 0; m < p.Length; m++)
                        {
                            d = intP[m] * intP[m];
                            hasil1 += d;
                            hasil = 0;
                            if (hasil1 > 9)
                            {
                                goto ulangan;
                            }
                        }
                    }
                }
                hasil += hasil1;

                if (hasil == 1)
                {
                    Console.WriteLine($"{j} is the number one");
                    hasil = 0;
                    hasil1 = 0;
                    j += 1;
                }
                else
                {
                    input = input + 1;
                    hasil = 0;
                    hasil1 = 0;
                    j += 1;
                }

            }

        }
        static void soal6()
        {
            //buat array penampungan pemeriksaan
            Console.WriteLine("masukkan input: ");
            string input = Console.ReadLine().ToLower();
            char[] arrInput = input.ToCharArray();

            string alphabet = "abcdefghijklmnopqrstuvwxyz ";
            char[] arrAlphabet = alphabet.ToCharArray();
            char[] arrinput = new char[input.Length];

            char kata;
            int x = 0;

            for (int i = 0; i < input.Length; i++)
            {
                kata = input[i];
                int idx = Array.IndexOf(arrAlphabet, kata);
                if (idx != -1)
                {
                    x += 0;
                }
                else
                {
                    x += 1;
                }
            }
            Console.WriteLine(x);
            if (x != 0)
            {
                Console.WriteLine("Kalimat ini bukan pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini adalah pangram");
            }
        }
        static void soal8()
        {
            Console.WriteLine("masukkan Input: ");
            string input = Console.ReadLine();
            char[] arrInput = input.ToCharArray();
            int[] intInput = Array.ConvertAll(arrInput, c => (int)Char.GetNumericValue(c));
            int k = 0;
            int hasil = 0;
            int g = 0;

            for (int j = 0; j < input.Length; j++)
            {

                if (intInput[j] > k)
                {
                    k = intInput[j];
                }
            }
            for (int i = 0; i < input.Length; i++)
            {
                g = intInput[i];
                if (g == k)
                {
                    hasil++;
                }
            }

            Console.WriteLine(hasil);
        }
        static void soal11()
        {
            Console.Write("masukkan input i: ");
            string i = Console.ReadLine();
            int p = Convert.ToInt32(i);
            char[] arrInput = i.ToCharArray();
            //int[] intInput = Array.ConvertAll(arrInput, c => (int)Char.GetNumericValue(c));

            Console.Write("masukkan input j: ");
            int j = int.Parse(Console.ReadLine());

            Console.Write("masukkan input k: ");
            int k = int.Parse(Console.ReadLine());
            int l = 0;
            string a ;
            char n;
            int hasil = 0;

            for (int m = p; m <= j; m++)
            {
                a = m.ToString();
                char[] klm = a.ToCharArray();
                Array.Reverse(klm);
               string  y = new string (klm);
                int z = int.Parse(y);
                hasil = (m - z);
                if (hasil%k==0)
                {
                    
                    Console.WriteLine(m);
                }
            }


        
    } 
        static void soal7()
        {
            int[,] arrInput = new int[3, 3];
            int y = 0;
            int z = 0;
            int hasil = 0;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write($"masukkan array ke-{i},{j}: ");
                    arrInput[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            y = arrInput[0, 0] + arrInput[1, 1] + arrInput[2, 2];
            z = arrInput[0, 2] + arrInput[1, 1] + arrInput[2, 0];
            hasil = y - z;
            Console.WriteLine(hasil);
            //Console.WriteLine(arr[0,2]);
        }
        static void soal3()
        {
            Console.WriteLine("keranjang 1= ");
            int keranjang1 = int.Parse(Console.ReadLine());

            Console.WriteLine("keranjang 2= ");
            int keranjang2 = int.Parse(Console.ReadLine());

            Console.WriteLine("keranjang 3= ");
            int keranjang3 = int.Parse(Console.ReadLine());

            Console.Write("yang dibawa adalah keranjang  ");
            int keranjangBawa = int.Parse(Console.ReadLine());
            int hasil = 0;

            if (keranjangBawa == 1)
            {
                hasil = keranjang2 + keranjang3;
                Console.WriteLine($"sisa buah={hasil}");
            }
            else if (keranjangBawa == 2)
            {
                hasil = keranjang1 + keranjang3;
                Console.WriteLine($"sisa buah={hasil}");
            }
            else if (keranjangBawa == 3)
            {
                hasil = keranjang1 + keranjang3;
                Console.WriteLine($"sisa buah={hasil}");
            }
        }
        static void soal4()
        {
            //pake swtich case buat milih baju yg pengen diinput
            int y = 0;
            int laki = 0;
            int wanita = 0;
            int anak1 = 0;
            int bayi1 = 0;
        inputpilihan:
            if (y == 0)
            {
                Console.WriteLine("1. lelaki dewasa,2.wanita dewasa,3.anak,4.bayi");
                Console.Write("masukkan pilihan (1-4): ");
                int pilihan = int.Parse(Console.ReadLine());


                switch (pilihan)
                {
                    case 1:

                        Console.WriteLine("lelaki dewasa= ");
                        int lakiDewasa = int.Parse(Console.ReadLine());
                        laki += lakiDewasa;
                        break;
                    case 2:
                        Console.WriteLine("wanita dewasa= ");
                        int wanitaDewasa = int.Parse(Console.ReadLine());
                        wanita += wanitaDewasa;
                        break;
                    case 3:
                        Console.WriteLine("anak-anak= ");
                        int anak = int.Parse(Console.ReadLine());
                        anak1 += anak;
                        break;
                    case 4:
                        Console.WriteLine("bayi= ");
                        int bayi = int.Parse(Console.ReadLine());
                        bayi1 += bayi;
                        break;

                }
                Console.WriteLine("isi lagi? y/n");
                string ulang1 = Console.ReadLine();
                if (ulang1 == "n")
                {
                    y += 1;
                    goto inputpilihan;
                }
                else
                {
                    y += 0;
                    goto inputpilihan;
                }
            }
            else if (y == 1)
            {

                int hasil = 0;
                int wanitaDewasa1 = 0;

                wanitaDewasa1 = wanita * 2;
                anak1 *= 3;
                bayi1 *= 5;
                hasil = laki + wanitaDewasa1 + anak1 + bayi1;

                if (hasil % 2 != 2 && hasil > 10)
                {
                    hasil += wanita;
                    Console.WriteLine($"{hasil} baju");

                }
                else
                {
                    Console.WriteLine($"{hasil} baju");

                }

            }

        }

        static void soal5()
        {
            Console.WriteLine("tepung terigu = ");
            float tepung = float.Parse(Console.ReadLine());
            Console.WriteLine("gula pasir = ");
            float gula = float.Parse(Console.ReadLine());
            Console.WriteLine("susu murni = ");
            float susu = float.Parse(Console.ReadLine());
            Console.WriteLine("putih telur = ");
            float telur = float.Parse(Console.ReadLine());
            Console.WriteLine("untuk membuat berapa kue= ");
            int pukis = int.Parse(Console.ReadLine());

            tepung /= pukis;
            gula /= pukis;
            susu /= pukis;
            telur /= pukis;

            Console.WriteLine($"bahan untuk 1 buah pukis");
            Console.WriteLine($"tepung terigu = {tepung} gram");
            Console.WriteLine($"gula pasir = {gula} gram");
            Console.WriteLine($"susu murni = {susu} gram");
            Console.WriteLine($"putih telur = {telur} gram");
        }
        static void soal9()
        {
            Console.Write("masukkan input: ");
            string input = Console.ReadLine();
            string[] strInput = input.Split(",");

            string input1 = strInput[0];
            int y = Convert.ToInt32(strInput[1]);
            int u = 0;
            //1 5 3
            int hasil = 0;
            int kek = 0;
            for (int i = 0; i < input1.Length; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    hasil += int.Parse(input1[i].ToString());
                }
            }
            string h = hasil.ToString();
            char[] k = h.ToCharArray();
            int[] intInput = Array.ConvertAll(k, c => (int)Char.GetNumericValue(c));
            if (intInput.Length > 1)
            {
                for (int x = 0; x < intInput.Length; x++)
                {
                    u = intInput[x];
                    kek += u;

                }
                Console.WriteLine(kek);
            }
            else
            {
                Console.WriteLine(hasil);
            }

        }
        static void soal10()
        {
            Console.Write("masukkan pulsa: ");
            int pulsa = int.Parse(Console.ReadLine());
            int y = 0;
            int hasil = 0;
            int x = 0;
            int z = 0;
            int k = 0;
            int l = 0;
            if (pulsa > 10000)
            {
                x = pulsa - 10000;
                if (x > 30000)
                {
                    x -= 30000;
                }
                else
                {
                    x += 0;
                }
                if (x > 0)
                {
                    for (int i = 1; i <= x; i++)
                    {
                        y = i;
                        if (y % 1000 == 0)
                        {
                            y /= 1000;

                        }
                    }
                }
                l = y - 20;
                if (l > 0)
                {
                    y -= l;
                }
            }
            if (pulsa > 30000)
            {
                z = pulsa - 30000;
                if (z > 0)
                {
                    for (int i = 1; i <= z; i++)
                    {
                        k = i;
                        if (k % 1000 == 0)
                        {
                            k /= 1000;
                            k *= 2;
                        }
                    }
                }

            }
            hasil = y + k;
            Console.WriteLine($"poin = {hasil}");
        }
    }
}
