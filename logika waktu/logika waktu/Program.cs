﻿using System;

namespace logika_waktu
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Write("nomer 1,2,3,4,5,6,7 dan 8 : ");
            Console.Write("nomer ? ");
            string pilih = Console.ReadLine();
            switch (pilih)
            {
                case "1":
                    soal1();
                    break;
                case "2":
                    soal2();
                    break;
                case "3":
                    soal3();
                    break;
                case "4":
                    soal4();
                    break;
                case "5":
                    soal5();
                    break;
                case "6":
                    soal6();
                    break;
                case "7":
                    soal7();

                    break;
                case "8":
                    soal8();
                    break;
            }
        }
        static void soal1()
        {
            Console.Write("masukan jam : ");
            string jam = Console.ReadLine().ToLower();
            //dari index ke 0 di ambil 2
            string merubah = jam.Substring(0, 2);
            //mengambil pm
            string pm = jam.Substring(jam.Length - 2, 2);
            int intjam = int.Parse(merubah);
            if (pm == "pm")
            {
                intjam = intjam + 12;
                string hasil = intjam.ToString();
                jam = jam.Replace(merubah, hasil);
                jam = jam.Remove(jam.Length - 2);
                Console.WriteLine(jam);
            }
            else
            {
                jam = jam.Remove(jam.Length - 2);
                Console.WriteLine(jam);
            }
            //Console.Write(" masukan jam : ");
            //string jam = Console.ReadLine();
            //System.DateTime ubahjam = new System.DateTime(jam);
        }
        static void soal2()
        {
            double[] jarakCustomer = { 2, 0.5, 1.5, 0.3 };
            double totalJarak = 0;
            Console.WriteLine("Input customer ke- (1-4): ");
            int inputCustomer = int.Parse(Console.ReadLine());
            int bensin = 0;
            string jarakOutput = "";
            {

            }
            for (int i = 0; i < inputCustomer; i++)
            {
                if (i != inputCustomer - 1)
                {
                    totalJarak += jarakCustomer[i];
                    jarakOutput += jarakCustomer[i] + " + ";
                }
                else
                {
                    totalJarak += jarakCustomer[i];
                    jarakOutput += jarakCustomer[i];

                }
            }
            double ben = totalJarak / 2.5;
            bensin = (int)ben;
            if (ben != bensin)
            {
                bensin += 1;
            }
            Console.WriteLine($"Jarak tempuh : {jarakOutput} = {totalJarak}");
            Console.WriteLine($"Bensin {bensin} Liter");
        }
        static void soal3()
        {
            Console.WriteLine("masukkan input hari: ");
            string inputhari = Console.ReadLine();
            string[] strinputhari = inputhari.Split("/");
            int[] arrinputhari = Array.ConvertAll(strinputhari, int.Parse);

            Console.WriteLine("masukkan input jam: ");
            string inputjam = Console.ReadLine();
            string[] strjam = inputjam.Split(".");
            int[] arrjam = Array.ConvertAll(strjam, int.Parse);

            Console.WriteLine("masukkan tgl keluar: ");
            string keluar = Console.ReadLine();
            string[] strKeluar = keluar.Split("/");
            int[] arrKeluar = Array.ConvertAll(strKeluar, int.Parse);
            float hasil = 0;
            int jam = 0;
            int jam1 = 0;

            Console.WriteLine("masukkan input jam keluar: ");
            string inputJamKeluar = Console.ReadLine();
            string[] strInputJamKeluar = inputJamKeluar.Split(".");
            int[] arrInputJamKeluar = Array.ConvertAll(strInputJamKeluar, int.Parse);

            DateTime date1 = new DateTime(arrinputhari[2], arrinputhari[1], arrinputhari[0], arrjam[0], arrjam[1], 0);
            Console.WriteLine(date1);
            DateTime date2 = new DateTime(arrKeluar[2], arrKeluar[1], arrKeluar[0], arrInputJamKeluar[0], arrInputJamKeluar[1], 0);
            Console.WriteLine(date2);
            System.TimeSpan perhitungan = date2 - date1;
            hasil = Convert.ToInt32(perhitungan.TotalMinutes);
            Console.WriteLine(hasil);
            jam = Convert.ToInt32(perhitungan.Hours * 60);
            jam1 = Convert.ToInt32(perhitungan.Hours);
            Console.WriteLine(jam);

            if (hasil > jam)
            {
                hasil = hasil + (((jam1 + 1) * 60) - hasil);
                hasil = (hasil / 60) * 3000;
                Console.WriteLine(hasil);
            }
            else
            {
                hasil = (hasil / 60) * 3000;
                Console.WriteLine(hasil);
            }
        }
        static void soal4()
        {
            Console.WriteLine("pinjaman buku : ");
            string input = Console.ReadLine();
            string[] strinput = input.Split("-");
            int[] arrinput = Array.ConvertAll(strinput, int.Parse);
            Console.WriteLine("tanggal pengembalian : ");
            string peminjamanan = Console.ReadLine();
            string[] strpeminjaman = peminjamanan.Split("-");
            int[] arrpeminjaman = Array.ConvertAll(strpeminjaman, int.Parse);
            int hasil = 0;
            int[] arrhasil = new int[arrinput.Length];

            DateTime date1 = new DateTime(arrinput[2], arrinput[1], arrinput[0]);
            DateTime date2 = new DateTime(arrpeminjaman[2], arrpeminjaman[1], arrpeminjaman[0]);
            System.TimeSpan kembali = date2 - date1;
            hasil = (kembali.Days - 3) * 500;
            Console.WriteLine(hasil);
            //Console.WriteLine(kembali);

        }
        static void soal5()
        {
            Console.WriteLine("Tanggal Mulai : ");
            string input = Console.ReadLine();//mm/dd/yyyy 12:00:00 AM
            DateTime datetime12 = DateTime.Parse(input);
            Console.WriteLine("masukan hari libur : ");
            int iputharilibur = int.Parse(Console.ReadLine());
            DateTime tglOutput = datetime12;
            int waktuBelajar = 10;
            int simpan = 0;
            // int loop = 2;
            bool isTrue = true;
            int i = 0;

            while (isTrue)
            {
                i++;
                tglOutput = datetime12.AddDays(i);
                if (tglOutput.DayOfWeek == DayOfWeek.Saturday || tglOutput.DayOfWeek == DayOfWeek.Sunday || i == iputharilibur)
                {
                    continue;
                }
                else
                {
                    simpan++;
                }

                if (simpan == waktuBelajar)
                {
                    isTrue = false;
                }
            }
            Console.WriteLine(tglOutput.ToString("MMMM dd, yyyy"));
        }
        static void soal6()
        {
            string abc = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char[] arrabc = abc.ToCharArray();
            Console.WriteLine("masukan sandi/password : ");
            string input = Console.ReadLine();
            Console.WriteLine("Masukan rotasi : ");
            int putar = int.Parse(Console.ReadLine());
            string encryptKalimat = "";
            for (int i = 0; i < input.Length; i++)
            {
                char kata = input[i];
                int idx = Array.IndexOf(arrabc, kata);
                if (idx != -1)
                {
                    encryptKalimat += arrabc[idx + putar];
                }
                else
                {
                    encryptKalimat += input[i];
                }
            }
            Console.WriteLine($"hasil encrypt : {encryptKalimat}");
        }
        static void soal7()
        {
            Console.Write("masukkan input ");
            int input = int.Parse(Console.ReadLine());
            int angka = 0;
            for (int i = 0; i < (input * 3) - 1; i++)
            {
                Console.Write(angka + " ");
                angka++;
                if (angka % input == 0)
                {
                    Console.WriteLine();

                }
            }
            Console.Write(angka + " ");
        }
        static void soal8()
        {
            Console.WriteLine("========================= : ");
            Console.WriteLine("=======pendaftaran======= : ");
            Console.WriteLine("masukan nama anda : ");
            string nama = Console.ReadLine();
            Console.WriteLine("masukan password anda : ");
            string pasword = Console.ReadLine();
            if (pasword == "made")
            {
                Console.WriteLine("Anda Berhasil Masuk");
            }
            else
            {
                Console.WriteLine("Password Anda Salah");
            }
        }
    }

}
