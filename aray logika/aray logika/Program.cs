﻿using System;

namespace aray_logika
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("nomer 1,2,3,4,5,6,7,8,9,10 : ");
            Console.Write("silahkan pilih nomer ? ");
            string pilih = Console.ReadLine();

            switch (pilih)
            {
                case "1":
                    Console.WriteLine("ini soal 1");
                    soal1();
                    break;
                case "2":
                    Console.WriteLine("ini soal 2");
                    soal2();
                    break;
                case "3":
                    Console.WriteLine("ini soal 3");
                    soal3();
                    break;
                case "4":
                    Console.WriteLine("ini soal 4");
                    soal4();
                    break;
                case "5":
                    soal5();
                    Console.WriteLine("ini soal 5");
                    break;
                case "6":
                    Console.WriteLine("ini soal 1");
                    soal6();
                    break;
                case "7":
                    Console.WriteLine("ini soal 2");
                    soal7();
                    break;
                case "8":
                    Console.WriteLine("ini soal 3");
                    soal8();
                    break;
                case "9":
                    Console.WriteLine("ini soal 4");
                    soal9();
                    break;
                case "10":
                    soal10();
                    Console.WriteLine("ini soal 5");
                    break;
            }
        }
        static void soal1()
        {
            //input data
            Console.WriteLine("uang andi : ");
            int uang = int.Parse(Console.ReadLine());
            Console.WriteLine("baju : ");
            string B = (Console.ReadLine());
            Console.WriteLine("celana : ");
            string C = (Console.ReadLine());
            //cara split
            string[] Hb = B.Split(",");
            string[] Hc = C.Split(",");
            //merubah string menjadi integer
            int[] hargabaju = Array.ConvertAll(Hb, int.Parse);
            int[] hargacelana = Array.ConvertAll(Hc, int.Parse);

            int cukup = 0;
            int hasil;
            for (int i = 0; i < hargabaju.Length; i++)
            {
                hasil = hargabaju[i] + hargacelana[i];
                if (hasil <= uang && hasil >= cukup)
                {
                    cukup = hasil;
                }
            }
            Console.WriteLine(cukup);

        }
        static void soal2()
        {
            //input data
            Console.WriteLine("uang andi : ");
            int uang = int.Parse(Console.ReadLine());
            Console.WriteLine("baju : ");
            string B = (Console.ReadLine());
            Console.WriteLine("celana : ");
            string C = (Console.ReadLine());
            //cara split
            string[] Hb = B.Split(",");
            string[] Hc = C.Split(",");
            //merubah string menjadi integer
            int[] hargabaju = Array.ConvertAll(Hb, int.Parse);
            int[] hargacelana = Array.ConvertAll(Hc, int.Parse);
            int cukup = 0;
            int hasil;
            for (int i = 0; i < hargabaju.Length; i++)
            {
                for (int j = 0; j < hargacelana.Length; j++)
                {
                    hasil = hargabaju[i] + hargacelana[j];
                    if (hasil <= uang && hasil >= cukup)
                    {
                        cukup = hasil;
                    }
                }
            }
            Console.WriteLine(cukup);
        }
        static void soal3()
        {
            {
                string inputArray;
                int input;
                //   int temp = 0;
                int simpan = 0;
                Console.WriteLine("Masukan Array : ");
                inputArray = Console.ReadLine();
                Console.WriteLine("Rotasi : ");
                input = int.Parse(Console.ReadLine());

                string[] arrayString = inputArray.Split(",");
                int[] arrayInt = Array.ConvertAll(arrayString, int.Parse);

                //[6,7,0,1,5]
                for (int i = 1; i <= input; i++)
                {

                    //untuk menyimpan nilai
                    simpan = arrayInt[0];
                    for (int j = 0; j < arrayInt.Length; j++)
                    {
                        if (j < arrayInt.Length - 1)
                        {
                            arrayInt[j] = arrayInt[j + 1];
                            // arrayint[3] = arrayint[4]
                            //=7
                        }
                        else
                        {
                            arrayInt[j] = simpan;
                        }
                    }
                    Console.Write(i + ". ");
                    foreach (var output in arrayInt)
                    {
                        Console.Write(output + ". ");
                    }
                    Console.WriteLine("");
                }
            }
        }
        static void soal4()
        {
            //input data
            Console.WriteLine("TOtal Menu : ");
            int menu = int.Parse(Console.ReadLine());
            Console.WriteLine(" Makanan alergi index ke- : ");
            int alergi = int.Parse(Console.ReadLine());
            Console.WriteLine(" Harga Menu : ");
            string harga = (Console.ReadLine());
            Console.WriteLine(" Uang elsa : ");
            int Uang = int.Parse(Console.ReadLine());
            //cara split 
            string[] intharga = harga.Split(",");
            //merubah dari string ke int 
            int[] Bharga = Array.ConvertAll(intharga, int.Parse);
            int kembali = 0;
            int hasil = 0;
            int mkn_elsa = 0;
            //menggunakan for
            for (int i = 0; i < Bharga.Length; i++)
            {
                hasil += Bharga[i];
            }
            hasil -= Bharga[alergi];
            mkn_elsa = hasil / 2;
            Console.WriteLine("elsa harus membayar : " + mkn_elsa);
            kembali = Uang - mkn_elsa;
            if (kembali == 0)
            {
                Console.WriteLine("uang elsa pas");
            }
            else if (kembali > 0)
            {
                Console.WriteLine("uang elsa kembali : " + kembali);
            }
            else if (kembali < 0)
            {
                Console.WriteLine("uang elsa kurang : " + kembali);
            }
            //foreach (int i in Bharga)
            //{
            //    hasil += i;
            //}
            //hasil -= Bharga[alergi];
            //Console.WriteLine("Elsa harus membayar: " + hasil);
            //kembali = hasil - Uang;
            //if(kembali==0)
            //{
            //    Console.WriteLine("uang pas");
            //}
            //else if(kembali>=0)
            //{
            //    Console.WriteLine("kembalian "+kembali);
            //}
            //else if (kembali <= 0)
            //{
            //    Console.WriteLine("kurang " + kembali);
            //}
        }
        static void soal5()
        {
            {

                Console.WriteLine("Input kalimat : ");
                string kalimat = Console.ReadLine().ToLower();
                int panjangString;
                panjangString = kalimat.Length;


                char[] vocal = new char[panjangString];
                char[] konsonan = new char[panjangString];
                for (int i = 0; i < panjangString; i++)
                {
                    if (kalimat[i] == 'a' || kalimat[i] == 'i' || kalimat[i] == 'u' || kalimat[i] == 'e' || kalimat[i] == 'o')
                    {
                        vocal[i] = kalimat[i];
                    }
                    else
                    {
                        konsonan[i] = kalimat[i];
                    }
                }
                Array.Sort(vocal);
                Array.Sort(konsonan);
                string vocalOutput = new string(vocal);
                string konsonanOutput = new string(konsonan);

                Console.WriteLine($"Vocal : {vocalOutput}");
                Console.WriteLine($"konsonan: {konsonanOutput}");

            }
        }
        static void soal6()
        {

        }
        static void soal7()
        {
            {
                Console.WriteLine("Masukan kalimat : ");
                string inputKalimat = Console.ReadLine();

                string[] arrKalimat = inputKalimat.Split(" ");
                for (int i = 0; i < arrKalimat.Length; i++)
                {
                    string kalimat = arrKalimat[i];
                    char[] ch = new char[kalimat.Length];
                    for (int j = 0; j < kalimat.Length; j++)
                    {
                        if (j == 0 || j == kalimat.Length - 1)
                        {
                            ch[j] = kalimat[j];
                        }
                        else
                        {
                            ch[j] = '*';
                        }
                    }
                    kalimat = new string(ch);
                    Console.Write(kalimat + " ");
                }
            }
        }
        static void soal8()
        {

        }
        static void soal9()
        {
            Console.WriteLine();
            Console.Write("Masukkan kata: ");
            string kata = Console.ReadLine();
            char[] katakata = kata.ToCharArray();
            int panjang = kata.Length - 1;
            bool palindrome = true;

            for (int i = 0; i <= panjang; i++)
            {
                katakata[i] = kata[i];
                if (katakata[i] != katakata[panjang])
                {
                    Console.WriteLine("No");
                    palindrome = false;
                    break;
                }
                else
                {
                    panjang--;
                }
            }

            if (palindrome == true)
            {
                Console.WriteLine("Yes");
            }
        }
        static void soal10()
        {
            Console.WriteLine();
            Console.Write("Masukkan Nilai: ");
            string nilai = Console.ReadLine();
            string[] nilai2 = nilai.Split(",");
            int[] intNilai = Array.ConvertAll(nilai2, int.Parse);
            int[] total = new int[intNilai.Length];
            int[] arrNilai = new int[intNilai.Length];
            int[] pangkasNilai = new int[intNilai.Length];

            for (int i = 0; i < intNilai.Length; i++)
            {
                arrNilai[i] = intNilai[i];
                pangkasNilai[i] = arrNilai[i] % 5;
                if (arrNilai[i] < 40)
                {
                    arrNilai[i] = arrNilai[i];
                }
                else if (pangkasNilai[i] == 3)
                {
                    arrNilai[i] = arrNilai[i] + 2;
                }
                else if (pangkasNilai[i] == 4)
                {
                    arrNilai[i] = arrNilai[i] + 1;
                }
                else if (pangkasNilai[i] == 5)
                {
                    arrNilai[i] = arrNilai[i];
                }
                else if (pangkasNilai[i] < 3)
                {
                    arrNilai[i] = arrNilai[i];
                }
                else if (arrNilai[i] < 40)
                {
                    arrNilai[i] = arrNilai[i];
                }
                Console.WriteLine(arrNilai[i]);
            }

            Console.ReadKey();
            Console.Clear();
        }
    }
}
